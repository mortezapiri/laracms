<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', '\App\Site\Home\Controllers\HomeController@index', function () {
	return view( 'welcome' );
} )->name( 'home' );

Route::get( '/', function () {
	return view( 'home' );
} )->name( 'home' );

Route::get( '/admin', [ 'middleware' => 'admin' ], function () {
	return view( 'admin.dashboard.index' );
} );

Route::group( [ 'prefix' => 'admin', 'namespace' => 'admin', 'middleware' => 'admin' ], function () {
	Route::get( '/urlshortner', 'UrlShortnerController@generate' )->name( 'admin.urlshortner' );
	Route::post( '/urlshortner', 'UrlShortnerController@store' )->name( 'admin.urlshortner' );
	Route::get( '/urlshortner/list', 'UrlShortnerController@show' )->name( 'admin.urlshortner.list' );
	Route::get( '/redirect/{code}', 'UrlShortnerController@redirect' );
} );

Route::group( [ 'prefix' => 'admin', 'middleware' => 'Authorize' ], function () {
	Route::resource( 'users', '\App\Admin\Users\Controllers\UsersController', [ 'as' => 'admin' ] );
	Route::resource( 'permissions', '\App\Admin\Permissions\Controllers\PermissionController', [ 'as' => 'admin' ] );
	Route::resource( 'roles', '\App\Admin\Roles\Controllers\RolesController', [ 'as' => 'admin' ] );
//	Route::resource( 'agencies', '\App\Admin\Agencies\Controllers\AgencieController', [ 'as' => 'admin' ] );
	Route::resource( 'posts', '\App\Admin\Posts\Controllers\PostsController', [ 'as' => 'admin' ] );
	Route::post( 'post/like', '\App\Admin\Posts\Controllers\PostsController@like', [ 'as' => 'admin' ] )->name( 'admin.post.like' );
//	Route::get( '/agencies/{id}/tour', '\App\Admin\Agencies\Controllers\AgencieController@toure', [ 'as' => 'admin' ] )->name( 'create.tour' );
	Route::resource( 'tours', '\App\Admin\Tours\Controllers\ToursController', [ 'as' => 'admin' ] );
	Route::get( '/', '\App\Admin\Dashboard\Controllers\DashboardController@index' )->name( 'admin.general.index' );
	Route::resource( '/products', '\App\Admin\Products\Controllers\ProductsController', [ 'as' => 'admin' ] );
	Route::post( '/products/get-attributes', '\App\Admin\Products\Controllers\ProductsController@getAttributes' );
	Route::post( '/products/get-options', '\App\Admin\products\Controllers\productsController@getOptions' );
	Route::resource( '/categories', '\App\Admin\Categories\Controllers\CategoriesController', [ 'as' => 'admin' ] );
	Route::resource( '/attributes', '\App\Admin\Attributes\Controllers\AttributesController', [ 'as' => 'admin' ] );
	Route::resource( '/options', '\App\Admin\Options\Controller\OptionsController', [ 'as' => 'admin' ] );
	Route::resource( '/ads', '\App\admin\Ads\Controllers\AdsController', [ 'as' => 'admin' ] );
	Route::post( '/ads/get-attributes', '\App\Admin\Ads\Controllers\AdsController@getAttributes' );
	Route::get( '/ads/waiting ', '\App\admin\Ads\Controllers\AdsController@waiting ', [ 'as' => 'admin' ] );
} );


Route::get( 'tags/{slug}', 'TagsController@index' )->name( 'tags.index' );

Route::group( [ 'prefix' => 'dashboard', 'namespace' => 'dashboard', 'middleware' => 'Authorize' ], function () {
	Route::get( '/', '\App\Admin\Tags\Controllers\IndexController@index' )->name( 'dashboard.general.index' );
} );

// Authentication Routes...
Route::get( 'login', '\App\General\Auth\Controllers\LoginController@showLoginForm' )->name( 'login' );
Route::post( 'login', '\App\General\Auth\Controllers\LoginController@login' );
Route::post( 'logout', '\App\General\Auth\Controllers\LoginController@logout' )->name( 'logout' );

// Registration Routes...
Route::get( 'register', '\App\General\Auth\Controllers\RegisterController@showRegistrationForm' )->name( 'register' );
Route::post( 'register', '\App\General\Auth\Controllers\RegisterController@register' );

// Password Reset Routes...
Route::get( 'password/reset', '\App\General\Auth\Controllers\ForgotPasswordController@showLinkRequestForm' );
Route::post( 'password/email', '\App\General\Auth\Controllers\ForgotPasswordController@sendResetLinkEmail' );
Route::get( 'password/reset/{token}', '\App\General\Auth\Controllers\ResetPasswordController@showResetForm' );
Route::post( 'password/reset', '\App\General\Auth\Controllers\ResetPasswordController@reset' );

Route::resource( '/products', '\App\Site\Products\Controllers\ProductsController' );
Route::post( '/product/{id}/comment', '\App\Site\Products\Controllers\ProductsController@comment' )->name( 'site.product.comment' );
Route::get( '/crawler', '\App\Site\Products\Controllers\CrawlerController@index' );

Route::get( '/test', 'TestController@index' );

Route::get( '/news', '\App\Site\Posts\Controllers\PostsController@index' );

Route::post( '/news/{id}/comment', '\App\Site\Posts\Controllers\PostsController@comment' )->name( 'site.post.comment' );
//
Route::get( '/news/{id}-{slug}', '\App\Site\Posts\Controllers\PostsController@show' )->name( 'news.show' );
Route::get( '/news/{id}-', '\App\Site\Posts\Controllers\PostsController@show' );
Route::get( '/news/{id}', '\App\Site\Posts\Controllers\PostsController@show' );
//
Route::post( '/comment', [
	'as'   => 'site.comment',
	'uses' => '\App\Site\Comment\Controllers\CommentsController@store'
] );

Route::resource( '/register', '\App\Site\Register\Controllers\RegisterController', [ 'as' => 'site' ] );



Route::group( [ 'prefix' => 'profile', 'middleware' => 'Authorize' ], function () {
	Route::resource( '/', '\App\Profile\Controllers\ProfileController', [ 'as' => 'site' ] );
	Route::resource( '/course', '\App\Profile\Courses\Controllers\CourseController', [ 'as' => 'site' ] );
} );


//Route::get( '/profile', function () {
//
//		return view( 'profile.dashboard.index');
//});
