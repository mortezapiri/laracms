<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model {

	protected $fillable = [ 'name' ];

	public function products() {
		return $this->belongsToMany( Product::class );
	}

	public function categories() {
		return $this->belongsToMany( Category::class );
	}

	public function ads() {
		return $this->belongsToMany( Ads::class );
	}

}
