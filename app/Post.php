<?php

namespace App;

use App\Contracts\IHittable;
use App\Helpers\GeneralHelpers;
use App\Traits\Hittable;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements IHittable {

	use Hittable;

	protected $fillable = [ 'title', 'body', 'published_at', 'user_id', 'tags', 'slug', 'hits', 'likes' ];

	public function tags() {
		return $this->belongsToMany( Tag::class );
	}

	public function user() {
		return $this->belongsTo( User::class );
	}

	public function comments() {
		return $this->morphMany(Comment::class,'commentable');
	}

	public function getPublishedAtAttribute( $value ) {

		$v = Verta::instance( $value );

		return $v->format( "Y-m-d H:i:s" );

	}

	public function setPublishedAtAttribute( $value ) {
		$v  = Verta::parse( $value );
		$this->attributes['published_at'] = $v->DateTime();
	}

	public function setSlugAttribute( $value ) {

		if ( isset( $value ) ) {

			$this->attributes['slug'] = GeneralHelpers::str_slug( $value );

		} else {
			$title = $this->attributes['title'];
			$this->attributes['slug'] = GeneralHelpers::str_slug( $title );
		}
	}

}