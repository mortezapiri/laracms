<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SaveCache extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */

	protected $signature = 'laracms:save-cache';


	/**
	 * The console command description.
	 *
	 * @var string
	 */

	protected $description = 'Save Cache Data To DataBase';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */

	public function handle() {

		$posts = \App\Post::get();


		foreach ( $posts as $post ) {

			$post->hits = \Cache::get( $post->getHitsCacheKey() );

			$post->save();

		}


		$this->info( 'hits posts saved' );

		try {

			$products = \App\Product::get();


			foreach ( $products as $product ) {

				$product->hits = \Cache::get( $product->getHitsCacheKey() );

				$product->save();

			}
		} catch ( \Exception $e ) {
			$this->error( $e->getMessage(), ' - ' . $e->getFile() . '#' . $e->getLine() );
		}

	}

}