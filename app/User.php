<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable {

	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [
		'id',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	public function courses() {
		return $this->belongsToMany( Course::class );
	}

	public function roles() {
//		return $this->belongsToMany(Role::class,'user_roles','user_id','role_id');
		return $this->belongsToMany( Role::class );
	}

	public function canSee( $routeName ) {

		$permissionName = $this->translateRouteToPermissionName( $routeName );

		return in_array( $permissionName, $this->roles->pluck( 'permissions' )->collapse()->pluck( 'name' )->unique()->toArray() );
	}

	private function translateRouteToPermissionName( $routeName ) {
		$routeNameArray = explode( '.', $routeName );
		$action         = $routeNameArray[2];
		$permissionName = $routeNameArray[0] . '.' . $routeNameArray[1];
		switch ( $action ) {
			case 'create':
			case 'store':
				$permissionName .= '.create';
				break;
			case 'edit':
			case 'update':
				$permissionName .= '.update';
				break;
			case 'destroy':
				$permissionName .= '.delete';
				break;
			case 'index':
			case 'show':
			default:
				$permissionName .= '.read';
		}

		return $permissionName;
	}

	public function permissions() {
		return $this->hasManyThrough( Permission::class, Role::class );
	}

	public function posts() {
		return $this->hasMany( Post::class );
	}

	public function getNameAttribute( $value ) {
		return ucfirst( $value );
	}

	public function getLastLoginAttribute( $value ) {
		return Carbon::parse( $value )->format( 'Y.m.d' );
	}

}