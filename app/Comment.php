<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	public $timestamps = false;
	protected $fillable = ['body','commentable_type','commentable_id'];

	public function commentable() {
		$this->morphTo();
    }
}
