<?php

namespace App\Site\Posts\Controllers;

use App\Jobs\ProcessHit;
use App\Post;
use App\Providers\ViewServiceProvider;
use App\Site\Comments\Form\CommentForm\CommentForm;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class PostsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$current_time = Carbon::now()->toDateTimeString();

		$posts = Post::where( 'published_at', '<=', $current_time )->paginate( 1 );

		return view( 'SitePosts::index', compact( 'posts', 'current_time' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @param null $slug
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id, $slug = null ) {

		$post = Post::findOrFail( $id );

		ProcessHit::dispatch( $post );

		if ( $post->slug != $slug ) {
			return \Redirect::route( 'news.show', [ 'id' => $post->id, 'slug' => $post->slug ] );
		}

		$publushed_at = $post->published_at;

		$tags = $post->tags;

		$current_time = Carbon::now()->toDateTimeString();

		$roleName = Auth::user();

		if ( $publushed_at >= $current_time ) {
			abort( 404 );
		}

		$changeDate = \verta( $publushed_at );


		return view( 'SitePosts::show', compact( 'post', 'roleName', 'changeDate', 'tags', 'form' ) );
	}


	public function comment( Request $request, $id ) {

		$post = Post::findOrFail( $id );

		$comment = $post->comments->create( $request->only( 'body' ) );

		return redirect()->route( 'post.show', [ 'id' => $post->id ] );

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {

	}

}
