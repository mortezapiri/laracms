@extends('layouts.app')

@section('content')

    <div class="container">

        @foreach($posts as $post)

            <div class="panel panel-success">
                <div class="panel-heading">

{{--                    <a href="{{ route('posts.show',$post->id) }}">{{ $post->title  }}</a>--}}

                    <span>{{$post->title}}</span>

                    <span style="float: left">
                    منتشر شده توسط
                        {{$post->user->name}}
                    </span>
                </div>
                <div class="panel-body">
                    {{$post->body}}
                </div>
            </div>

        @endforeach

        {!! $posts->render() !!}

    </div>

@endsection