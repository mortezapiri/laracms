@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="jumbotron">


            <h1>{{ $product->title }}</h1>
            دسته بندی
            <h3>{{ $product->category->name }}</h3>

            <label>

                @foreach($product->options as $item)

                    <select class="form-control form-control-sm">

                        @foreach($item->data as $item2)

                            <option value="' {{$item2->value}} . '"> {{$item2->text}}</option>

                        @endforeach

                    </select>

                @endforeach

            </label>

            <br>


            @include('Partials.comment')

        </div>


    </div>


@endsection

