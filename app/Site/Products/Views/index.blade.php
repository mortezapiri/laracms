@extends('layouts.app')

@section('content')

    <div class="container">

        @foreach ($products->chunk(4) as $chunk)
            <div class="row">
                @foreach ($chunk as $product)
                    <div class="col-xs-4">
                        <a href="{{route('products.show',$product->id)}}">
                            {{ $product->title . '- ( ' . $product->category . ' )' . $product->attributes . $product->options}}
                        </a>
                    </div>
                @endforeach
            </div>
        @endforeach
        {{--{!! $products->render() !!}--}}
    </div>

@endsection