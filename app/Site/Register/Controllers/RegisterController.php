<?php

namespace App\Site\Register\Controllers;

use App\Http\Controllers\Controller;
use App\Site\Register\Forms\RegisterForm;
use App\Site\Register\Repositories\RegisterRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller {


	public function create() {

		$form = \FormBuilder::create(RegisterForm::class, [
			'method' => 'POST',
			'url'    => route( 'site.register.store' ),
		] );

		return view('SiteRegister::create',compact('form'));

	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store( Request $request , RegisterRepository $repository) {

		$user = $repository->create($request->only('user_name','email','password', 'role_ids'));

		Auth::login($user);

		return redirect()->route('home');

	}
}
