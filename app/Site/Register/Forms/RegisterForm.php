<?php

namespace App\Site\Register\Forms;

use App\Role;
use Kris\LaravelFormBuilder\Form;

use App\User;

class RegisterForm extends Form
{
    public function buildForm()
    {
	    $this->add('user_name', 'text', [
		    'label' => 'نام کاربری',
	    ] )
	         ->add( 'email', 'email', [
		         'label' => 'ایمیل',
		         'required' => true,
	         ] )
		    ->add('password','password',[
			    'label' => 'پسورد'
		    ])
		    ->add('password_confirmation','password',[
			    'label' => 'پسورد'
		    ])
	         ->add( 'role_ids', 'select', [
		         'label'   => 'ثبت نام به عنوان',
//		         'rules' => 'required',
//		         'required' => 'min:1',
		         'attr' => ['class' => 'form-control'],
		         'multiple' =>true,
//		         'expanded' => true,
		         'choices' => $this->getRoles(),
	         ])

	         ->add( 'submit', 'submit', [
		         'label' => 'ایجاد',
	         ]);
    }

	/**
	 * @return mixed
	 */
	private function getRoles() {

		$roles = Role::where('name','=','استاد')
			->orWhere('name','=','هنرجو')->get()->pluck('name','id')->toArray();

		return $roles;

	}
}
