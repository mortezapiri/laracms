<?php

namespace App\Site\Comment\Controllers;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller {

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store( Request $request ) {

		$comment = new Comment( [
			'body'             => $request->input( 'body' ),
			'commentable_id'   => $request->input( 'id' ),
			'commentable_type' => $request->input( 'type' )
		] );

		$type = $request->input( 'type' );

		$model = $type::findOrFail( $request->input( 'id' ) );

		$model->comments()->save( $comment );

		return redirect()->back();

	}
}
