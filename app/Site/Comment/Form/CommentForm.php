<?php

namespace App\Site\Comment\Form;

use Kris\LaravelFormBuilder\Form;

class CommentForm extends Form
{
    public function buildForm()
    {

        $this->add('body','text',[
        	'label' => 'send comment'
        ]);
        $this->add('submit','submit',[
        	'label'  => 'send'
        ]);

    }
}
