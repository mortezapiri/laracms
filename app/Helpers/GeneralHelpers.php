<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class GeneralHelpers{

	public static function str_slug($string, $limit=100, $separator='-'){
		$string = strtolower($string);
		$string = str_replace('‌', ' ', $string);
		$string = Str::words($string, $limit, '');
		$string = mb_ereg_replace('([^آ-ی۰-۹a-z0-9_]|-)+', $separator, $string);
		$string = strtolower($string);
		return trim($string, $separator);
	}

}