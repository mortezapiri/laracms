<?php

namespace App\Composers;

use Illuminate\View\View;

class CommentFormComposer {

	public function compose( View $view ) {

		$route     = \Request::route();
		$routeName = $route->getName();

		switch ($routeName) {
			case 'products.show':
				$type = 'App\Product';
				$id = $route->parameters()['product'];
				break;
			case 'news.show':
				$type = 'App\Post';
				$id = $route->parameters()['id'];
				break;
		}
		$view
			->with('type', $type)
			->with('id', $id);
	}
}