<?php

namespace App\Jobs;

use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TestTitle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $post;

	/**
	 * Create a new job instance.
	 *
	 * @param Post $post
	 */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
    public function handle()
    {

    	$title = $this->post->title;

    	$title = 'Test Job';

    	$title->save();
    }
}
