<?php

namespace App\Jobs;

use App\Contracts\IHittable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessHit {
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var IHittable
	 */
	public $hittable;
	/**
	 * Create a n
	 * ew job instance.
	 *
	 * @param IHittable $hittable
	 */
	public function __construct( IHittable $hittable ) {
		$this->hittable = $hittable;
	}
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {
		$this->hittable->hit();
	}

}