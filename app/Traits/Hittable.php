<?php

namespace App\Traits;

use Cache;

trait Hittable {

	public function getHitsCacheKey() {
		return 'hits.' . $this->getTable() . '.' . $this->id;
	}

	public function hit() {
		$key = $this->getHitsCacheKey();

		if ( Cache::has( $key ) ) {
			Cache::increment( $key, 1 );
		} else {
			Cache::forever( $key, $this->hits );
		}
	}

	public function getHitsCountAttribute() {
		$key = $this->getHitsCacheKey();
		if ( \Cache::has( $key ) ) {
			return Cache::get( $key );
		} else {
			return $this->hits;
		}
	}
}
