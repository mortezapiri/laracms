@extends('ProfileLayout::index')

@section('content')

    <a href="{{ route('admin.posts.create')  }}"
       style="margin:6px 15px;padding:9px 50px;background-color:#d61577;border-color:#ad0b5d;font-weight:bold;color:#FFF"
       target="_blank">افزودن پست جدید</a>

    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست پست ها</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>نام دوره</th>
                                <th>وضعیت</th>
                                <th style="text-align: center">ادیت</th>
                            </tr>
                            </thead>

                            @foreach($courses as $course)

                                <a href="#">{{$course->title}}</a>

                            @endforeach


                            <tfoot>
                            <tr>
                                <th>نام دوره</th>
                                <th>وضعیت</th>
                                <th>ویرایش</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

