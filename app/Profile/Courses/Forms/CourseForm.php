<?php

namespace App\Profile\Courses\Forms;

use Kris\LaravelFormBuilder\Form;

class CourseForm extends Form
{
    public function buildForm()
    {
        $this
	        ->add( 'title', 'text', [
		        'label'    => 'تیتر دوره',
//				'required' => true,
	        ] )
	        ->add( 'content', 'textarea', [
		        'label'    => 'توضیحات',
//				'required' => true,
	        ] )
	        ->add( 'video_count', 'text', [
		        'label'    => 'تعداد ویدئو ها',
//				'required' => true,
	        ] )
	        ->add( 'files', 'file', [
		        'label'    => 'آپلود فایل',
//				'required' => true,
	        ] )
	        ->add('submit','submit',[
	        	'label' => 'ارسال برای بازبینی',
		        'attr' => [
		        	'btn' => 'primary'
		        ]
	        ]);

    }
}
