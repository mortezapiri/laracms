<?php

namespace App\Profile\Courses\Controllers;

use App\Course;
use App\Http\Controllers\Controller;
use App\Profile\Courses\Forms\CourseForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$user = Auth::user()->id;

    	$courses = Course::findOrFail($user)->toArray();

    	dd($courses['title']);

    	return view('ProfileCourse::index',compact('courses'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $form = \FormBuilder::create(CourseForm::class, [
		    'method' => 'POST',
		    'url'    => route( 'site.course.store' )
	    ] );

	    return view( 'ProfileCourse::create', compact( 'form' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

	    $course = Course::create([
		    'title' => $request->input('title'),
		    'content' => $request->input('content'),
		    'video_count' => $request->input('video_count'),
		    'teacher_name' => Auth::user()->name,
		    'user_id'  => Auth::user()->id,
		    'likes' => 0,
		    'status' => 'waiting'
	    ]);

	    return redirect()->back()->with('success','دوره با موفقیت برای بازبینی ارسال شد');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
