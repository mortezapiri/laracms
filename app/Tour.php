<?php

namespace App;

use App\Contracts\IHittable;
use App\Traits\Hittable;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model implements IHittable
{
	use Hittable;

	protected $fillable = [ 'title', 'content', 'agencie_id' ];


	public function agencies() {

		return $this->belongsTo( Agencie::class );

	}

}
