<?php

namespace App\Repositories;


use App\Http\Controllers\realtimeController;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository {

	public function create($data) {

		$role_ids = $data['role_ids'];

		$user = User::create( [
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => Hash::make($data['password']),
		] );

		$user->roles()->attach($role_ids);

		return $user;

	}


	public function update( User $user, $data ) {

		$role_ids = $data['role_ids'];
		unset($data['role_ids']);

		$user->update($data);

		$user->roles()->sync($role_ids);

		return $user;

	}

}