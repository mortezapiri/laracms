<?php

namespace App\Contracts;

interface IHittable {
	public function hit();
}