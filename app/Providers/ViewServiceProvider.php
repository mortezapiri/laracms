<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    	view()->addNamespace('SiteHome', base_path('/app/Site/Home/Views'));
    	view()->addNamespace('SitePosts', base_path('/app/Site/Posts/Views'));
	    view()->addNamespace('AdminDashboard', base_path('/App/Admin/Dashboard/Views'));
	    view()->addNamespace('AdminPosts', base_path('/App/Admin/Posts/Views'));
    	view()->addNamespace('AdminAgencies', base_path('/App/Admin/Agencies/Views'));
    	view()->addNamespace('AdminPermissions', base_path('/App/Admin/Permissions/Views'));
    	view()->addNamespace('AdminRoles', base_path('/App/Admin/Roles/Views'));
    	view()->addNamespace('AdminTours', base_path('/App/Admin/Tours/Views'));
    	view()->addNamespace('AdminUsers', base_path('/App/Admin/Users/Views'));
    	view()->addNamespace('GeneralAuth', base_path('/App/General/Auth/Views'));
    	view()->addNamespace('GeneralPass', base_path('/App/General/Auth/Views/Passwords'));
    	view()->addNamespace('AdminProducts', base_path('/App/Admin/Products/Views'));
    	view()->addNamespace('AdminCategories', base_path('/App/Admin/Categories/Views'));
    	view()->addNamespace('AdminAttributes', base_path('/App/Admin/Attributes/Views'));
    	view()->addNamespace('SiteProducts', base_path('/App/Site/Products/Views'));
    	view()->addNamespace('AdminProducts', base_path('/App/Admin/Products/Views'));
    	view()->addNamespace('AdminOptions', base_path('/App/Admin/Options/Views'));
    	view()->addNamespace('AdminAds', base_path('/App/Admin/Ads/Views'));
    	view()->addNamespace('SiteRegister', base_path('/App/Site/Register/Views'));
    	view()->addNamespace('Profile', base_path('/App/Profile/Views/dashboard'));
    	view()->addNamespace('ProfileLayout', base_path('/App/Profile/Views/layout'));
    	view()->addNamespace('ProfilePartials', base_path('/App/Profile/Views/Partials'));
    	view()->addNamespace('ProfileCourse', base_path('/App/Profile/Courses/Views'));

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
