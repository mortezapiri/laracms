<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Post;

class Tag extends Model
{
	protected $fillable = ['name','id'];

//	protected $casts = [
//		'name' => 'array',
//	];

	public function posts() {
		return $this->belongsToMany(Post::class);
    }

//	public function setNameAttribute($name) {
//		$this->attributes['name'] = $name;
//		$this->attributes['slug'] = str_slug($name);
//    }

	public function getRouteKey() {
		return 'slug';
	}


}
