<?php

namespace App\Admin\Dashboard\Controllers;

use App\Admin\Dashboard\Forms\StatusForm;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller {
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index( Request $request ) {


		$totalsPosts = [
			'posts' => Post::count()
		];

		$totalsUsers = [
			'users' => User::count()
		];


//		$action = $request->input('action');
//
//		switch ($action) {
//			case 'online':
//				$return = 'آنلاین است';
//				redirect()->back();
//				break;
//			case 'offline':
//				$return = 'آفلاین است';
//				redirect()->route('admin.general.index');
//				break;
//			default:
//				$return = 'آفلاین است';
//			redirect()->back();
//		}


		return view( 'AdminDashboard::index', compact( 'totalsPosts', 'totalsUsers') );

	}


}
