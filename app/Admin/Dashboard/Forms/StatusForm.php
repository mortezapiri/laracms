<?php

namespace App\Admin\Dashboard\Forms;

use Kris\LaravelFormBuilder\Form;

class StatusForm extends Form
{
    public function buildForm()
    {
	    $this->add('online ', 'submit', [
		         'label' => 'آنلاین',
		         'attr' => [
			         'class' => 'btn btn-success',
			         'default_value' => 'online',
			         'name' => 'action',
			         'value' => 'online'
		         ]
	         ])
	         ->add('offline', 'submit', [
		         'label' => 'آفلاین',
		         'attr' => [
			         'class' => 'btn btn-danger',
			         'default_value' => 'offline',
			         'name' => 'action',
			         'value' => 'offline'
		         ]
	         ]);

    }
}
