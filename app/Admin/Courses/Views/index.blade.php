@extends('layout.admin')

@section('content')


    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست دسته بندی ها</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام دسته بندی</th>
                                <th>ویرایش</th>
                            </tr>
                            </thead>
                            @foreach($categories as $category)

                                <tr>
                                    <td > {{  $category->id }} </td>
                                    <td > {{  $category->name }} </td>
                                    <td>
                                        <a href="{{ route('admin.categories.edit',$category->id) }}">
                                            <li class="glyphicon glyphicon-edit"></li>
                                        </a>
                                    </td>

                                </tr>

                            @endforeach

                            <tfoot>
                            <tr>
                                <th>شناسه</th>
                                <th>نام دسته بندی</th>
                                <th>ویرایش</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection