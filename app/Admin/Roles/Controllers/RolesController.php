<?php

namespace App\Admin\Roles\Controllers;

use App\Http\Controllers\Controller;
use App\Forms\PostForm;
use App\Admin\Roles\Forms\RoleForm;
use App\Admin\Roles\Requests\RoleRequest;
use App\Post;
use App\Role;
use Collective\Html\FormBuilder;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\App;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Psy\Test\Exception\RuntimeExceptionTest;
use RealRashid\SweetAlert\Facades\Alert;


class RolesController extends Controller {


	use FormBuilderTrait;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		Alert::alert( 'Title', 'Message', 'Type' );

		$roles = Role::all();

		return view( 'AdminRoles::index', compact( 'roles' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$form = $this->form( RoleForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.roles.store' )
		] );

		return view( 'AdminRoles::create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( RoleRequest $request ) {

		$permission_ids = $request->input( 'permissions_ids', [] );


		$role = Role::create( [
			'name' => $request['name'],
		] );

		$role->permissions()->attach($permission_ids);

		alert()->success( 'Role Created', 'Successfully' );

		return redirect()->route( 'admin.roles.edit', [ 'id' => $role->id ] );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$role = Role::findOrFail( $id );

		$model = $role->toArray();

		$model['permissions_ids'] = $role->permissions->pluck( 'id' )->toArray();

		$form = \FormBuilder::create( RoleForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.roles.update', [ 'id' => $role->id ] ),
			'model'  => $model
		] );

		return view( 'AdminRoles::create', compact( 'form' ) );

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( RoleRequest $request, Role $role ) {

		$permission_ids = $request['permissions_ids'];

		unset( $request['permissions_ids'] );

		$role->update( $request->all() );

		$role->permissions()->sync( $permission_ids );

		return redirect()->back();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
