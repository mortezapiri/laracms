<?php

namespace App\Admin\Roles\Forms;

use App\Permission;
use Kris\LaravelFormBuilder\Form;

class RoleForm extends Form
{
    public function buildForm()
    {
        $this->add('name','text',[
        'label' => 'نام نقش'
        ])
        ->add('permissions_ids','choice', [
        	'label' => 'تعیین پرمیشن',
		    'multiple' => true,
//		    'expanded' => true,
		    'choices' => $this->getPermissions(),
	        'attr' => ['class' => 'form-control select2']
	     ])
        ->add('submit','submit',[
        	'label' => 'ایجاد نقش جدید'
        ]);
    }

	private function getPermissions() {

		$permissions = Permission::get()->pluck('name','id')->toArray();

		return $permissions;
    }
}

