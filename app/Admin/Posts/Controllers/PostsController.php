<?php

namespace App\Admin\Posts\Controllers;

use App\admin\Posts\Forms\PostForm;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessHit;
use App\Post;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$posts = Post::with( 'user' )->latest()->get();

		$current_time = Carbon::now()->toDateTimeString();

		return view( 'AdminPosts::index', compact( 'posts', 'current_time' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$form = \FormBuilder::create( PostForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.posts.store' ),
		] );

		return view( 'AdminPosts::create', compact( 'form' ) );

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$user_id = Auth::user()->id;

		$tag_names = $request->input( 'tag_ids' );

//		dd($tag_names);

		$data = array_merge( $request->only( 'title', 'body', 'published_at', 'slug' ), [
			'user_id' => $user_id,
			'hits'    => 0,
			'likes'   => 0
		] );
		$post = Post::create( $data );

		$tags = collect( [] );

		foreach ( $tag_names as $tag_name ) {
			$tags->push( Tag::firstOrCreate( [ 'name' => $tag_name ] )->id );
		}
		$post->tags()->sync( $tags );


		flash()->overlay( 'پست جدید با موفقیت ساخته شد', 'پست ایجاد شد' );

		return redirect()->route( 'admin.posts.edit', [ 'id' => $post->id ] );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {

		$post = Post::findOrFail( $id );

		ProcessHit::dispatch($post);

		return view( 'AdminPosts::show', compact( 'post' ) );

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$post = Post::findOrFail( $id );

		$model = $post->toArray();

		$model['tag_ids'] = $post->tags->pluck( 'id' )->toArray();

		$form = \FormBuilder::create( PostForm::class, [
				'method' => 'PUT',
				'url'    => route( 'admin.posts.update', [ 'id' => $post->id ] ),
				'model'  => $model
			]
		);

		return view( 'AdminPosts::edit', compact( 'form' ) );

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, Post $post ) {

//		dd($request->all());

		$tag_ids = $request['tag_ids'];

		$sync = collect( [] );
		foreach ( $tag_ids as $tag_id ) {

			if ( is_numeric( $tag_id ) ) {
				$tag = Tag::find( $tag_id );
			} else {
				$tag = Tag::firstOrCreate( [
					'name' => $tag_id,
				] );
			}
			if ( $tag ) {
				$sync->push( $tag->id );
			}
		}

		$post->update( $request->all() );

		$post->tags()->sync( $sync );

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {

		$post = Post::find( $id );

		$post->delete();

		return redirect()->back();

	}

	public function like( Request $request ) {

		$post_id = $request->input( 'post_id' );
		if ( $post_id && intval( $post_id ) > 0 ) {
			$post = Post::find( $post_id );
			$post->increment( 'likes' );

			return [
				'success' => true,
				'message' => 'لایک شد'
			];
		}

		return [
			'success' => false,
			'message' => 'لطفا مجدد امتحان کنید'
		];

	}


}