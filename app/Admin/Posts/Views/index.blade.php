@extends('layout.admin')

@section('content')

    <a href="{{ route('admin.posts.create')  }}"
       style="margin:6px 15px;padding:9px 50px;background-color:#d61577;border-color:#ad0b5d;font-weight:bold;color:#FFF"
       target="_blank">افزودن پست جدید</a>

    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست پست ها</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>نویسنده</th>
                                <th style="text-align: center">ادیت</th>
                            </tr>
                            </thead>

                            @foreach($posts as $post)

                                {{--@if($post->published_at < $current_time)--}}

                                    <tr>
                                        <td><a href="{{route('admin.posts.show',$post)}}">{{$post->title}}</a></td>
                                        <td>{{$post->user->name}}</td>
                                        <td style="text-align: center">
                                            <a href="{{ route('admin.posts.edit',$post->id) }}">
                                                <li class="glyphicon glyphicon-edit"></li>
                                            </a>
                                            <a href="{{  route('admin.posts.destroy',$post->id)  }}">
                                                <li class="glyphicon glyphicon-trash"></li>
                                            </a>
                                        </td>
                                    </tr>

                                {{--@endif--}}

                            @endforeach


                            <tfoot>
                            <tr>
                                <th>عنوان</th>
                                <th>نام نویسنده</th>
                                <th>ویرایش</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

