@extends('layout.admin')

@section('content')

    <a href="{{ route('admin.posts.create')  }}"
       style="margin:6px 15px;padding:9px 50px;background-color:#d61577;border-color:#ad0b5d;font-weight:bold;color:#FFF"
       target="_blank">افزودن پست جدید</a>
    <br>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">

                    {!! form($form) !!}

                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->

@endsection