@extends('layout.admin')

@section('content')

    <section class="content">

        <!-- Default box -->

        <div class="box">
            <div class="box-header with-border">

                <h3 class="box-title">{{ $post->title }}</h3>

                <a href="#" data-fid="{{$post->id}}" class="btn_like fa fa-fw fa-heart " style="color: red;"></a>

                <h2>{{$post->slug}}</h2>
                تعداد بازدید
                <h2>{{$post->hits}}</h2>



                <h5>منتشر شده در {{$post->published_at}}</h5>

                @unless($post->tags->isEmpty())
                    <h5>Tags:</h5>
                    <ul>
                        @foreach($post->tags as $tag)

                            <li><a href="#">{{$tag->name}}</a></li>

                        @endforeach
                    </ul>
                @endunless



                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="جمع شود">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                            title="حذف">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                {{ $post->body }}
            </div>


            <!-- /.box-body -->
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>


@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function ($) {
            $(document).on('click', '.btn_like', function (event) {

                event.preventDefault();
                var $this = $(this);
                var post_id = $this.data('fid');
                $.ajax({
                    url: '/admin/post/like',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        post_id: post_id,
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (response) {
                        if (response.success) {
                            swal({
                                text: response.message,
                                title: response.message,
                                icon: 'success'
                            });
                        }
                    },
                    erorr: function () {

                    }
                });

            });
        });

    </script>
@endsection