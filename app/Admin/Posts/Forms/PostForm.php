<?php

namespace App\Admin\Posts\Forms;

use App\Permission;
use App\Post;
use App\Tag;
use Hekmatinasser\Verta\Facades\Verta;
use Kris\LaravelFormBuilder\Form;
use Symfony\Component\HttpFoundation\Request;

class PostForm extends Form {
	public function buildForm() {
		$this->add( 'title', 'text', [
			'label' => 'عنوان'
		] )
			->add('slug','text',[
				'label' => 'لینک یکتا'
			])
		     ->add( 'body', 'textarea', [
			     'label' => 'توضیحات'
		     ] )
			->add( 'image', 'file', [
				'label' => 'افزودن تصویر'
			] )
		     ->add( 'published_at', 'text', [
		     	'label' => 'تاریخ انتشار',
			     'attr' => [ 'class' => 'form-control date form_datetime' ],
		     ] )
		     ->add( 'tag_ids', 'choice', [
			     'multiple' => true,
			     'label' => 'افزودن تگ',
			     'choices'  => $this->getTags(),
			     'attr'     => [ 'class' => 'select2 form-control' ]
//			     'selected' => $this->getModel() ? $this->getModel()->tags()->pluck('name')->toArray() : [],
		     ] )
		     ->add( 'submit', 'submit', [
			     'label' => 'ایجاد'
		     ] );
	}


	private function getDate() {

		$model = $this->getModel();

		return $model ? $model->published_at : 'Not Set';

	}

	private function getTags() {

		$tags = Tag::get()->pluck('name','id')->toArray();


		return $tags;

	}

}
