@extends('layout.admin')

@section('content')


    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست دسته بندی ها</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام دسته بندی</th>
                            </tr>
                            </thead>
                            @foreach($attributes as $attribute)

                                <tr>
                                    <td > {{  $attribute->id }} </td>
                                    <td > {{  $attribute->name }} </td>

                                </tr>

                            @endforeach

                            <tfoot>
                            <tr>
                                <th>شناسه</th>
                                <th>نام دسته بندی</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection