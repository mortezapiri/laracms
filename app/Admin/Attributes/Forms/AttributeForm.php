<?php

namespace App\Admin\Attributes\Forms;

use Kris\LaravelFormBuilder\Form;

class AttributeForm extends Form
{
    public function buildForm()
    {
	    $this->add('name','text',[
		    'label' => 'عنوان'
	    ]);
	    $this->add('submit','submit',[
		    'label' => 'ذخیره ویژگی'
	    ]);
    }
}
