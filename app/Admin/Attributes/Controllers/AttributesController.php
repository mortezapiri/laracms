<?php

namespace App\Admin\Attributes\Controllers;

use App\Admin\Attributes\Forms\AttributeForm;
use App\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributesController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$attributes = Attribute::get();

		return view( 'AdminAttributes::index', compact( 'attributes' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$form = \FormBuilder::create( AttributeForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.attributes.store' ),
		] );

		return view( 'AdminAttributes::create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$attribute = Attribute::create( $request->only( 'name' ) );

		return redirect()->route( 'admin.attributes.edit', [ 'id' => $attribute->id ] );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$attribute = Attribute::findOrFail( $id );

		$form = \FormBuilder::create( AttributeForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.attributes.update', [ 'id' => $attribute->id ] ),
			'model'  => $attribute
		] );

		return view( 'AdminAttributes::create', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$attribute = Attribute::findOrFail( $id );

		$attribute->update( $request->only( 'name' ) );

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
