<?php

namespace App\Admin\Ads\Forms;


use App\Category;
use Kris\LaravelFormBuilder\Form;

class AdsForm extends Form {
	public function buildForm() {
		$this
			->add( 'title', 'text', [
				'label'    => 'عنوان',
//				'required' => true,
			] )
			->add( 'content', 'textarea', [
				'label'    => 'توضیحات',
//				'required' => true,
				'attr'     => [ 'class' => 'form-control mce-tinymce mce-rtl mce-container mce-panel' ],
			] )
			->add( 'category_id', 'choice', [
				'label'    => 'انتخاب دسته بندی',
//				'required' => true,
				'choices'  => $this->getCategories(),
			] )
			->add( 'phone', 'text', [
				'label'    => 'موبایل',
//				'required' => true,
			] )
			->add( 'price', 'text', [
				'label'    => 'قیمت',
//				'required' => true,
			] )
			->add( 'email', 'text', [
				'label'    => 'ایمیل',
//				'required' => true,
			] )
			->add( 'website', 'text', [
				'label'    => 'وب سایت',
//				'required' => true,
			] )
			->add( 'city', 'text', [
				'label'    => 'شهر',
//				'required' => true,
			] )
			->add( 'state', 'text', [
				'label'    => 'استان',
//				'required' => true,
			] )
			->add( 'images', 'file', [
				'label'    => 'آپلود تصویر',
//				'required' => true,
			] )
			->add( 'aparat_link', 'text', [
				'label'    => 'لینک آپارات',
//				'required' => true,
			] )
			->add( 'kind_of', 'text', [
				'label'    => 'نوع درخواست',
//				'required' => true,
			] )
			->add( 'submit', 'submit', [
				'label' => 'ذخیره آگهی'
			] );
	}

	public function getCategories() {

		$categories = Category::get()->pluck( 'name', 'id' )->toArray();

		return $categories;

	}

}
