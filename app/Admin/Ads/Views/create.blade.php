@extends('layout.admin')

@section('content')



    <!-- Main content -->
    <section class="content">
        <div class="row">

            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    {!! form_start($form) !!}
                    {!! form_row($form->title) !!}
                    {!! form_row($form->content) !!}
                    {!! form_row($form->category_id) !!}
                    <div id="attributes"></div>
                    {!! form_rest($form) !!}
                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->

@endsection

{{--@section('scripts')--}}

    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$.ajax({--}}
                {{--type: 'post',--}}
                {{--url: '/admin/ads/get-attributes',--}}
                {{--data: {--}}
                    {{--category_id: $('#category_id option:selected').val(),--}}
                    {{--ad_id: 1,--}}
                    {{--_token: '{{ csrf_token() }}',--}}
                {{--},--}}
                {{--success: function (data) {--}}
                    {{--$('#attributes').html(data);--}}
                {{--}--}}
            {{--});--}}
            {{--$('#category_id').change(function () {--}}
                {{--$.ajax({--}}
                    {{--type: 'post',--}}
                    {{--url: '/admin/ads/get-attributes',--}}
                    {{--data: {--}}
                        {{--category_id: $('#category_id option:selected').val(),--}}
                        {{--ad_id: {{ isset($item) ? $item->id : 0 }},--}}
                        {{--_token: '{{ csrf_token() }}'--}}
                    {{--},--}}
                    {{--success: function (data) {--}}
                        {{--$('#attributes').html(data);--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}


    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('.select2').select2({--}}
                {{--tags: true--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}


{{--@endsection--}}