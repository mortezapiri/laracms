<?php

namespace App\Admin\Ads\Controllers;


use App\Admin\Ads\Forms\AdsForm;
use App\Ads;
use App\Category;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$form = \FormBuilder::create( AdsForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.ads.store' ),
		] );

		return view( 'AdminAds::create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

//		dd( $request->all() );

		$user_id = Auth::user()->id;

		$ads = Ads::create([
			'title' => $request->input('title'),
			'slug' => $request->input('title'),
			'content' => $request->input('content'),
			'category_id' => $request->input('category_id'),
			'phone' => $request->input('phone'),
			'price' => $request->input('price'),
			'email' => $request->input('email'),
			'website' => $request->input('website'),
			'city' => $request->input('city'),
			'state' => $request->input('state'),
			'aparat_link' => $request->input('city'),
			'images' => $request->input('images'),
			'likes' => 0,
			'hits' => 0,
			'ip' => $request->ip(),
			'user_id' => $user_id,
			'expired_at' => Carbon::now(),
			'kind_of' => $request->input('kind_of'),
		]);

		return redirect()->back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	public function waiting() {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}


//	public function getAttributes( Request $request ) {
//
//
//		$category = Category::findOrFail( $request->input( 'category_id' ) );
//
//		$ad = Ads::find( $request->input( 'ad_id' ) );
//
//		$attributes = $category->attrs()->get()->map( function ( $attribute, $key ) use ( $ad ) {
//
//
//			$ret = '<div class="form-group"><label class="control-label">' . $attribute->name . '</label>
//			<input type="checkbox" class="flat-red" name="attr[' . $attribute->id . ']"';
//			if ( $ad ) {
//				$ret .= ' value="' . $attribute->name . '"';
//			}
//			$ret .= '></div>';
//
//			return $ret;
//
//		} )->toArray();
//
//		return implode( "\n", $attributes );
//	}



}
