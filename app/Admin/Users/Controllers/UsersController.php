<?php

namespace App\Admin\Users\Controllers;
use App\Admin\Users\Forms\UserForm;
use App\Http\Controllers\Controller;
use App\Admin\Users\Requests\UserRequest;
use App\User;
use App\Admin\Users\Repositories\UserRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends Controller {


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$users = User::all();

		return view( 'AdminUsers::list',compact('users'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$form = \FormBuilder::create( UserForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.users.store' )
		] );

		return view( 'AdminUsers::create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param ProductRepository $user_repository
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(UserRequest $request , UserRepository $repository) {

//		$form = \FormBuilderUserForm::class);
//
//		if (!$form->isValid()) {
//			return redirect()->back()->withErrors($form->getErrors())->withInput();
//		}
//		$form->redirectIfNotValid();

		$user = $repository->create($request->only('name','email','password', 'role_ids'));

		return redirect()->route('admin.users.edit',['id'=>$user->id]);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$user = User::findOrFail( $id );

		$model = $user->toArray();
		$model['role_ids'] = $user->roles->pluck( 'id' )->toArray();


		$form = \FormBuilder::create( UserForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.users.update', [ 'id' => $user->id ] ),
			'model'  => $model,
		] );

		return view( 'AdminUsers::edit',compact('form'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( $id, Request $request,UserRepository $repository) {

		$user = $repository->update( User::findOrFail( $id ), $request->only('name','email','password','role_ids'));

		return redirect()->route( 'admin.users.edit', [ 'id' => $user->id ] );

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {

	}



}
