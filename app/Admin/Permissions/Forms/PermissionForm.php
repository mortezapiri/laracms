<?php

namespace App\Admin\Permissions\Forms;

use App\Permission;
use App\Role;
use Kris\LaravelFormBuilder\Form;

class PermissionForm extends Form {

	public function buildForm() {

		$this->add( 'name', 'text', [
			'label'    => 'ایجاد پرمیشن',
			'rules'    => 'required',
			'required' => 'min:1',
		])
		->add('submit' , 'submit',[
			'label' => 'ساختن'
		]);
	}
}
