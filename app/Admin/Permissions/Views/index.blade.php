@extends('layout.admin')

@section('content')

    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست تور ها</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نقش</th>
                                <th style="text-align: center">ادیت</th>
                            </tr>
                            </thead>
                            @foreach($permissions as $permission)

                                <tr>
                                    <td > {{  $permission->id }} </td>
                                    <td > {{  $permission->name }} </td>


                                    <td style="text-align: center">
                                        <a href="{{ route('admin.permissions.edit',$permission->id) }}">
                                            <li class="glyphicon glyphicon-edit"></li>
                                        </a>
                                        {{--<a href="{{  route('admin.users.destroy',$role->id)  }}">--}}
                                        {{--<li class="glyphicon glyphicon-trash"></li>--}}
                                        {{--</a>--}}
                                    </td>

                                </tr>

                            @endforeach




                            <tfoot>
                            <tr>
                                <th>شناسه</th>
                                <th>نام تور</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection