<?php

namespace App\Admin\Products\Forms;

use App\Attribute;
use App\Category;
use Kris\LaravelFormBuilder\Form;

class ProductForm extends Form {
	public function buildForm() {
		$this->add( 'title', 'text', [
			'label' => 'عنوان'
		] );
		$this->add( 'category_id', 'choice', [
			'label'    => 'انتخاب دسته بندی',
			'choices' => $this->getCategories(),
		] );
//		->add('attribute_ids','choice', [
//			'label' => 'ویژگی ها',
//			'multiple' => true,
////		    'expanded' => true,
//			'choices' => $this->getAttributes(),
//			'attr' => ['class' => 'form-control select2']
//		]);
		$this->add( 'submit', 'submit', [
			'label' => 'ذخیره محصول'
		] );
	}

	public function getCategories() {

		$categories = Category::get()->pluck('name', 'id' )->toArray();

		return $categories;

	}


//	public function getAttributes() {
//
//		$attributes = Attribute::get()->pluck( 'name', 'id' )->toArray();
//
//		return $attributes;
//
//	}


}
