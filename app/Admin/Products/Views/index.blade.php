@extends('layout.admin')

@section('content')


    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست محصول ها</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نام محصول</th>
                                <th>ویرایش</th>
                            </tr>
                            </thead>
                            @foreach($products as $product)

                                <tr>
                                    <td> {{  $product->id }} </td>
                                    <td> {{  $product->title }} </td>
                                    <td>
                                        <a href="{{ route('admin.products.edit',$product->id) }}">
                                            <li class="glyphicon glyphicon-edit"></li>
                                        </a>
                                    </td>

                                </tr>

                            @endforeach

                            <tfoot>
                            <tr>
                                <th>شناسه</th>
                                <th>نام محصول</th>
                                <th>ویرایش</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection