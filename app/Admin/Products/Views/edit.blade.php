@extends('layout.admin')

@section('content')


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">


                    {!! form_start($form) !!}
                    {!! form_row($form->title) !!}
                    {!! form_row($form->category_id) !!}
                    <div id="attributes"></div>
                    <div id="options"></div>
                    {!! form_rest($form) !!}



                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->

@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            $.ajax({
                type: 'post',
                url: '/admin/products/get-attributes',
                data: {
                    category_id: $('#category_id option:selected').val(),
                    product_id: {{ isset($product) ? $product->id : 0 }},
                    _token: '{{ csrf_token() }}',
                },
                success: function (data) {
                    $('#attributes').html(data);
                }
            });
            $('#category_id').change(function () {
                $.ajax({
                    type: 'post',
                    url: '/admin/products/get-attributes',
                    data: {
                        category_id: $('#category_id option:selected').val(),
                        product_id: {{ isset($item) ? $item->id : 0 }},
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        $('#attributes').html(data);
                    }
                });
            });
            $.ajax({
                type: 'post',
                url: '/admin/products/get-options',
                data: {
                    category_id: $('#category_id option:selected').val(),
                    product_id: {{ isset($product) ? $product->id : 0 }},
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    $('#options').html(data);
                },
            });
            $('#category_id').change(function () {
                $.ajax({
                    type: 'post',
                    url: '/admin/products/get-options',
                    data: {
                        category_id: $('#category_id option:selected').val(),
                        product_id: {{ isset($item) ? $item->id : 0 }},
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        $('#options').html(data);
                    }
                });
            });


        });
    </script>
@endsection