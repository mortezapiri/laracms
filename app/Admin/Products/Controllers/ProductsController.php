<?php

namespace App\Admin\Products\Controllers;

use App\Admin\Products\Forms\ProductForm;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index() {

		$products = Product::get();

		return view( 'AdminProducts::index', compact( 'products' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$form = \FormBuilder::create( ProductForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.products.store' ),
		] );

		return view( 'AdminProducts::create', compact( 'form', 'attributes' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$product = Product::create( [
			'title'       => $request->input( 'title' ),
			'category_id' => $request->input( 'category_id' ),
			'hits' => 0 ,
		] );

		$data = [];

		foreach ( $request->get( 'attr' ) as $id => $attr ) {
			$data[ $id ] = [ 'value' => $attr ];
		}

		$product->attrs()->attach( $data );

//		unset($data);

		$optionsData = [];
		foreach ( $request->get( 'opt' ) as $id => $opt ) {
			$optionsData[ $id ] = [ 'data' => str_replace( "'", '"', $opt ) ];
		}

		$product->options()->attach( $optionsData );

		return redirect()->route( 'admin.products.edit', [ 'id' => $product->id ] );

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {

		$product = Product::find($id)->decorate();

//		return view('AdminProducts::show',compact('product'));

		return $product;

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$product = Product::findOrFail( $id );


		$model = $product->toArray();

		$model['attribute_ids'] = $product->attrs()->pluck( 'id' )->toArray();
//		$model['option_ids'] = $product->options()->pluck( 'id' )->toArray();

//		dd($model);

		$form = \FormBuilder::create( ProductForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.products.update', [ 'id' => $product->id ] ),
			'model'  => $model
		] );

		return view( 'AdminProducts::edit', compact( 'form', 'product' ) );

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$product = Product::findOrFail( $id );

//		dd($request->get('attr'));

		$product->update( $request->only( 'title', 'category_id' ) );

		$data = [];
		foreach ( $request->get( 'attr' ) as $id => $attr ) {
			$data[ $id ] = [ 'value' => $attr ];
		}
		$product->attrs()->sync( $data );


		$optionsData = [];
		foreach ( $request->get( 'opt' ) as $id => $opt ) {
			$optionsData[ $id ] = [ 'data' => str_replace( "'", '"', $opt ) ];
		}

		$product->options()->sync( $optionsData );


		\Cache::forget( 'product.' . $product->id );

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}

	public function getAttributes( Request $request ) {

//		return 'hello';

		$category = Category::findOrFail( $request->input( 'category_id' ) );

//		return $category;

		$product = Product::find( $request->input( 'product_id' ) );

		$attributes = $category->attrs()->get()->map( function ( $attribute, $key ) use ( $product ) {

			$ret = '<div class="form-group"><label class="control-label">' . $attribute->name . '</label>
			<input class="form-control" name="attr[' . $attribute->id . ']"';
			if ( $product ) {
				$ret .= ' value="' . $product->attrs->where( 'id', $attribute->id )->first()->pivot->value . '"';
			}
			$ret .= '></div>';

			return $ret;

		} )->toArray();

		return implode( "\n", $attributes );
	}

	/**
	 * @param Request $request
	 *
	 * @return string
	 */
	public function getOptions( Request $request ) {

		$categories = Category::find( $request->input( 'category_id' ) );

		$product = Product::find( $request->input( 'product_id' ) );

		$title = '<h4>آپشن ها</h4></br>';

		echo $title;

		$options = $categories->options()->get()->map( function ( $option, $key ) use ( $product ) {

			$ret = '<div class="form-group"><label class="control-label">' . $option->name . '</label>

			<input class="form-control" name="opt[' . $option->id . ']"';
			if ( $product ) {
				$ret .= ' value="' . str_replace( '"', "'", $product->options->where( 'id', $option->id )->first()->pivot->data ) . '"';
			}
			$ret .= '></div><hr>';

			return $ret;

		} )->toArray();

		return implode( "\n", $options );

	}

}