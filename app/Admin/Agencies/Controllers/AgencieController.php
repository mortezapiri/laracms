<?php
//
use App\Admin\Agencies\Controllers;
use App\admin\Agencies\Forms\AdsForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Agencie;
use App\Forms\ToursForm;
use Illuminate\Http\Request;

class AgencieController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {

		if ( ! Auth::check() ) {
			return redirect()->route( 'login' );

		}

	}

	public function index() {

		$agencie = Agencie::all();

		return view( 'AdminAgencies::index', compact( 'agencie' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */

	public function create() {

		$form = \FormBuilder::create( AdsForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.agencies.store' )
		] );

		return view( 'AdminAgencies::create', compact( 'form' ) );

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$agencie = Agencie::create( $request->only( 'title', 'about' ) );

//		dd($agencie);

		flash()->overlay( 'آژانس جدید با موفقیت ساخته شد', 'آژنس ساخته شد' );

		return redirect()->route( 'admin.agencie.edit', [ 'id' => $agencie->id ] );

	}

	public function toure( Request $request, $id ) {

		$agencie = Agencie::findOrFail( $id );

		$form = \FormBuilder::create( ToursForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.tours.store', [ 'id' => $agencie->id ] )
		] );

		return view( 'AdminTours::create', compact( 'form' ) );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {


		$agencie = Agencie::findOrFail( $id );

		$toures = $agencie->tours;


		return view( 'AdminAgencies::show', compact( 'agencie', 'toures' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$agencie = Agencie::findOrFail( $id );

		$form = \FormBuilder::create( AdsForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.agencies.update', [ 'id' => $agencie->id ] ),
			'model'  => $agencie,
		] );

		return view( 'AdminAgencies::edit', compact( 'form' ) );

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$agencies = Agencie::findOrFail( $id );

		$agencies->update( $request->only( 'title', 'about' ) );

		return redirect()->route( 'AdminAgencies::edit', [ 'id' => $agencies->id ] );


	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
