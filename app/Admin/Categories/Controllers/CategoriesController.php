<?php

namespace App\Admin\Categories\Controllers;

use App\Admin\Categories\Forms\CategoryForm;
use App\Category;
use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;

class CategoriesController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$categories = Category::get();

		return view( 'AdminCategories::index', compact( 'categories' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$form = \FormBuilder::create( CategoryForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.categories.store' ),
		] );

		return view( 'AdminCategories::create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

//		dd( $request->all() );

		$Attribute_ids = $request->input( 'attribute_ids', [] );
		$option_ids    = $request->input( 'option_ids', [] );

		$category = Category::create( [
			'name' => $request->input( 'name' )
		] );

		$category->options()->attach( $option_ids );
		$category->attrs()->attach( $Attribute_ids );

//		$category->options()->sync( $option );

		return redirect()->route( 'admin.categories.edit', [ 'id' => $category->id ] );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {

		$option = Category::with('options')->find($id)->toArray();

		dd($option['options']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$category = Category::findOrFail( $id );

		$model = $category->toArray();

		$model['attribute_ids'] = $category->attrs->pluck( 'id', 'name' )->toArray();

//	    dd($model);

		$form = \FormBuilder::create( CategoryForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.categories.update', [ 'id' => $category->id ] ),
			'model'  => $model
		] );

		return view( 'AdminCategories::create', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, Category $category ) {
		$attribute_ids = $request['attribute_ids'];

		unset( $request['attribute_ids'] );

		$category->update( $request->all() );

		$category->attrs()->sync( $attribute_ids );


		return redirect()->back();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}

	public function getOptions( Request $request ) {
		$ret = '	
				<label class="control-label" for="">options</label><br>
				<form action="" method="post">
				<input class="form-control" type="text">
				</form>
				';

		return $ret;
	}


}
