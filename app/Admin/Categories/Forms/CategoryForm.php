<?php

namespace App\Admin\Categories\Forms;

use App\Attribute;
use App\Option;
use Kris\LaravelFormBuilder\Form;

class CategoryForm extends Form {
	public function buildForm() {
		$this->add( 'name', 'text', [
			'label' => 'عنوان دسته بندی'
		] )
		     ->add( 'option_ids', 'choice', [
			     'label'    => 'آپشن ویژگی',
			     'multiple' => true,
			     'choices'  => $this->getOptions(),
			     'attr'     => [ 'class' => 'select2 form-control' ]
		     ] )
		     ->add( 'attribute_ids', 'choice', [
			     'label'    => 'تعیین ویژگی',
			     'multiple' => true,
			     'choices'  => $this->getAttributes(),
			     'attr'     => [ 'class' => 'select2 form-control' ]
		     ] )
		     ->add( 'submit', 'submit', [
			     'label' => 'ذخیره دسته بندی'
		     ] );
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		$attributes = Attribute::get()->pluck( 'name', 'id' )->toArray();

		return $attributes;
	}


	/**
	 * @return array
	 */
	public function getOptions() {
		$options = Option::get()->pluck( 'name', 'id' )->toArray();

		return $options;
	}

}
