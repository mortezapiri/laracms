@extends('layout.admin')

@section('content')


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
            @include('admin.Partials.alert')

            <!-- general form elements -->
                <div class="box box-primary">

                    {!! form($form) !!}

                    {{--{!! form_start($form) !!}--}}
                    {{--{!! form_row($form->name) !!}--}}
                    {{--<div id="options"></div>--}}
                    {{--{!! form_rest($form) !!}--}}

                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->
@endsection


{{--@section('scripts')--}}

    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$.ajax({--}}
                {{--type: 'post',--}}
                {{--url: '/admin/categories/get-options',--}}
                {{--data: {--}}

                    {{--_token: '{{ csrf_token() }}'--}}
                {{--},--}}
                {{--success: function (data) {--}}
                    {{--$('#options').html(data);--}}
                {{--},--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}

{{--@endsection--}}