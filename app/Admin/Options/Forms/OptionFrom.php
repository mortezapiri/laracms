<?php

namespace App\Admin\Options\Forms;

use Kris\LaravelFormBuilder\Form;

class OptionFrom extends Form
{
    public function buildForm()
    {
	    $this->add( 'name', 'text', [
		    'label'    => 'ایجاد آپشن',
	    ])
	         ->add('submit' , 'submit',[
		         'label' => 'ساختن'
	         ]);
    }
}
