<?php

namespace App\Admin\Options\Controller;

use App\Admin\Options\Forms\OptionFrom;
use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;

class OptionsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$form = \FormBuilder::create( OptionFrom::class, [
			'method' => 'POST',
			'url'    => route( 'admin.options.store' ),
		] );

		return view( 'AdminOptions::create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$option = Option::create( $request->only( 'name' ) );

		return redirect()->route( 'admin.options.edit', [ 'id' => $option->id ] );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$option = Option::findOrFail( $id );

		$form = \FormBuilder::create( OptionFrom::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.options.update', [ 'id' => $option->id ] ),
			'model'  => $option
		] );

		return view( 'AdminOptions::create', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$options = Option::findOrFail( $id );

		$options->update( $request->only( 'name' ) );

		return redirect()->back();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
