@extends('layout.admin')

@section('content')


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
            @include('admin.Partials.alert')

            <!-- general form elements -->
                <div class="box box-primary">

                    {!! form($form) !!}

                </div>
            </div>
        </div>
    </section>
    <!-- /.box -->

@endsection




