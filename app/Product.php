<?php

namespace App;

use App\Contracts\IHittable;
use App\Traits\Hittable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements IHittable {

	use Hittable;

	protected $fillable = [ 'title', 'category_id', 'hits' ];

	public function attrs() {
		return $this->belongsToMany( Attribute::class )->withPivot( 'value' );
	}

	public function category() {
		return $this->belongsTo( Category::class );
	}

	public function options() {
		return $this->belongsToMany( Option::class )->withPivot( 'data' );
	}

	public function comments() {
		return $this->morphMany(Comment::class,'commentable');
	}

	public function decorate() {

		$data = (object) [
			'id'         => $this->id,
			'title'      => $this->title,
			'category'   => (object) [
				'id'   => $this->category->id,
				'name' => $this->category->name,
			],
			'attributes' => [],
			'options'    => [],
			'comments'   =>[]
//			'hits'       => $this->hits,
		];
		foreach ( $this->attrs as $attr ) {
			$data->attributes[] = (object) [
				'id'    => $attr->id,
				'name'  => $attr->name,
				'value' => $attr->pivot->value,
			];
		}
		foreach ( $this->options as $id => $opt ) {
			$data->options[ $id ] = (object) [
				'name' => $opt->name,
				'data' => json_decode( $opt->pivot->data ),
			];
		}
		foreach ( $this->comments as $comment ) {
			$data->comments[] = (object) [
				'body' => $comment->body
			];
		}

		return $data;

	}

	public function getDetailsAttribute() {

		$that = $this;

		return \Cache::rememberForever( 'product.' . $this->id, function () use ( $that ) {
			return $that->decorate();
		} );

	}

	public function getListDetailsAttribute() {

		$details = $this->details;

		$listDetails = (object) [
			'id'         => $details->id,
			'title'      => $details->title,
			'category'   => $details->category->name,
			'attributes' => implode( ', ', collect( $details->attributes )->pluck( 'value' )->toArray() ),
			'options'    => implode( ', ', collect( $details->options )->pluck( 'text' )->toArray() )
		];

		return $listDetails;
	}

}