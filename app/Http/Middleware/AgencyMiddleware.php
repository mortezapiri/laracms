<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AgencyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $role = Auth::user()->roles()->where('name', 'manager')->exists();

	    if (! $role){
		    return redirect()->route('home');
	    }
        return $next($request);
    }
}
