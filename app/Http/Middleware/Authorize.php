<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//   	$user = Auth::user();
//   	$user->load('roles','roles.permissions');
//
//   	if(!$user->canSee($request->route()->getName())){
//   		return redirect()->route('home');
//   	}

        return $next($request);
    }
}
