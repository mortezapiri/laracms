<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;

class CountController extends Controller {
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {


		$totalsPosts = [
			'posts' => Post::count()
		];

		$totalsUsers = [
			'users' => User::count()
		];

		dd('ok');

		return view( 'AdminDashboard::index', compact( 'totalsPosts', 'totalsUsers' ) );

	}


}
