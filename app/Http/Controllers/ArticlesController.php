<?php

namespace App\Http\Controllers;

//use Collective\Html\FormBuilder;
use Illuminate\Http\Request;

use App\Forms\ArticleForm;

use App\Article;
use Illuminate\Routing\Route;

use kris\LaravelFormBuilder\FormBuilder;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param FormBuilder $formBuilder
	 *
	 * @return \Illuminate\Http\Response
	 */
//    public function create()
//    {
//
//
//
//	    $form = \FormBuilder::create(ArticleForm::class, [
//		    'method' => 'POST',
//		    'url' => route('article.store')
//	    ]);
//
//
//
//
//	    return view('articles.create',compact('form'));
//
//    }

	public function create(FormBuilder $formBuilder)
	{
		$form = $formBuilder->create('App\Forms\ArticleForm', [
			'method' => 'POST',
			'url' => route('article.store')
		]);

		return view('articles.create', compact('form'));
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $article=Article::create($request->only('title','body'));
//
//        return redirect()->route('article.edit',['id'=>$article->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
