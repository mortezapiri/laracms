<?php

namespace App\Http\Controllers\Site\Posts;

use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$current_time = Carbon::now()->toDateTimeString();

		$posts = Post::where( 'published_at', '<=', $current_time )->paginate( 1 );

		return view( 'site.posts.index', compact( 'posts', 'current_time' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @param null $slug
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id, $slug = null ) {

//		dd($slug);

		$post = Post::findOrFail( $id );

		if ( $post->slug != $slug ) {
			return \Redirect::route( 'site.news.show', [ 'id' => $post->id, 'slug' => $post->slug ] );
		}

//		dd($post);

		$publushed_at = $post->published_at;

		$tags = $post->tags;


//		dd($publushed_at);

		$current_time = Carbon::now()->toDateTimeString();

		$roleName = Auth::user();

		if ( $publushed_at >= $current_time ) {
			abort( 404 );
		}

		//$v = verta();

		//return $v->format('Y-n-j');

		$changeDate = \verta( $publushed_at );

		return view( 'site.posts.show', compact( 'post', 'roleName', 'changeDate', 'tags' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {

	}
}
