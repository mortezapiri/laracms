<?php

namespace App\Http\Controllers;

use App\Forms\PermissionForm;
use App\Http\Requests\PermissionRequest;
use App\Permission;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Role;

class PermissionController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$permissions = Permission::all();

		return view( 'admin.permissions.index', compact( 'permissions' ) );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$form = \FormBuilder::create( PermissionForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.permissions.store' ),
		] );

		return view( 'admin.permissions.create', compact( 'form' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

		$permission = Permission::create( $request->only( 'name' ) );

		flash()->overlay( 'پرمیشن جدید با موفقیت ساخته شد', 'پرمیشن ساخته شد' );

		return redirect()->route( 'admin.permissions.edit', [ 'id' => $permission->id ] );

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$permission = Permission::findOrFail( $id );

		$form = \FormBuilder::create( PermissionForm::class, [
			'method' => 'PUT',
			'url'    => route( 'admin.permissions.update', [ 'id' => $permission->id ] ),
			'model'  => $permission
		] );

		return view( 'admin.permissions.create', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$permission = Permission::findOrFail( $id );

		$permission->update( $request->only( 'name' ) );

		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
