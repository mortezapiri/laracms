<?php

namespace App\Http\Controllers;

use App\Forms\PostForm;
use App\Jobs\TestTitle;
use App\Permission;
use App\Post;
use App\Role;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Verta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Psy\Util\Str;
use GuzzleHttp\Client;

class PostsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

//		$api = file_get_contents('https://jsonplaceholder.typicode.com/posts');
//
//		$resualt =\GuzzleHttp\json_decode($api);
//
//		dd($resualt);

		$posts = Post::with( 'user' )->latest()->get();

		$current_time = Carbon::now()->toDateTimeString();

		return view( 'admin.posts.index', compact( 'posts', 'current_time' ) );

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$form = \FormBuilder::create( PostForm::class, [
			'method' => 'POST',
			'url'    => route( 'admin.posts.store' ),
		] );

		return view( 'admin.posts.create', compact( 'form' ) );

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {

//		dd($request->all());

//		$job = (new TestTitle($request->title))->onQueue('title');
//
//		$job->delay(Carbon::now()->addMinutes(1));
//
//		dispatch($job);


		$user_id = Auth::user()->id;

		$tag_names = $request->input( 'tag_ids' );

//		dd($tag_names);

		$data = array_merge( $request->only( 'title', 'body', 'published_at','slug','image' ), [
			'user_id' => $user_id,
		] );
		$post = Post::create( $data );

		$tags = collect( [] );

		foreach ( $tag_names as $tag_name ) {
			$tags->push( Tag::firstOrCreate( [ 'name' => $tag_name ] )->id );
		}
		$post->tags()->sync( $tags );

		flash()->overlay( 'پست جدید با موفقیت ساخته شد', 'پست ایجاد شد' );

		return redirect()->route( 'admin.posts.edit', [ 'id' => $post->id ] );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		$post = Post::findOrFail($id);

		return view( 'admin.posts.show', compact( 'post', 'changeDate') );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {

		$post = Post::findOrFail( $id );

		$model = $post->toArray();

//		dd($model);

		$model['tag_ids'] = $post->tags->pluck( 'id' )->toArray();

		$form = \FormBuilder::create( PostForm::class, [
				'method' => 'PUT',
				'url'    => route( 'admin.posts.update', [ 'id' => $post->id ] ),
				'model'  => $model
			]
		);

		return view( 'admin.posts.edit', compact( 'form' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, Post $post ) {

		$tag_ids = $request['tag_ids'];

		$sync = collect( [] );
		foreach ( $tag_ids as $tag_id ) {

			if ( is_numeric( $tag_id ) ) {
				$tag = Tag::find( $tag_id );
			} else {
				$tag = Tag::firstOrCreate( [
					'name' => $tag_id,
				] );
			}
			if ( $tag ) {
				$sync->push( $tag->id );
			}
		}

		$post->update($request->all());

		$post->tags()->sync( $sync );


		return redirect()->back();
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

		$post = Post::find( $id );

		$post->delete();

		return redirect()->back();

	}
}