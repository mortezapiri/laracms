<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

	protected $fillable =['title','content','teacher_name','likes','video_count','status','user_id'];

	public function users() {
		return $this->belongsToMany(User::class);
	}

	public function students() {
		return $this->belongsToMany(Student::class);
    }

	public function teachers() {
		return $this->belongsToMany(Teacher::class);
    }

}
