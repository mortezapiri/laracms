<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlShortners extends Model
{
	protected $guarded = ['url_id'];

	protected $primrykey = 'url_id';
}
