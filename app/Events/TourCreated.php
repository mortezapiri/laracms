<?php

namespace App\Events;

use App\Tour;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TourCreated
{
    use InteractsWithSockets, SerializesModels;

    public $tour;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Tour $tour)
    {
       $this->tour = $tour;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
