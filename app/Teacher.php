<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{


	public function students() {
		return $this->belongsToMany(Student::class);
    }


	public function courses() {
		return $this->belongsToMany(Course::class);
    }


}
