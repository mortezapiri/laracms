<?php

namespace App\Listeners;

use App\Events\TourCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplyTour
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TourCreated  $event
     * @return void
     */
    public function handle(TourCreated $event)
    {

	    $tour= $event->tour;

	    $tour->title = 'Listener';

	    $tour->save();

//	    dd($tour->agencie_id);


    }
}
