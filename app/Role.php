<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

//	protected $fillable = ['name'];
	protected $guarded = ['id'];
	public function users() {
//		return $this->belongsToMany(User::class,'user_roles','role_id','user_id');
		return $this->belongsToMany(User::class);
	}

	public function permissions() {
		return $this->belongsToMany(Permission::class);
	}


}
