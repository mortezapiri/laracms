<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $fillable = ['name'];

	public function products() {
		return $this->hasMany(Product::class);
	}

	public function attrs() {
		return $this->belongsToMany(Attribute::class);
	}

	public function options() {
		return $this->belongsToMany(Option::class);
	}

}
