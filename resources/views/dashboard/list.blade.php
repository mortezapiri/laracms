@include('admin.Partials.header')
@extends('layout.admin')
@include('admin.Partials.sidebar')

@section('title')
    لیست پرمیشن ها
@endsection

@include('admin.Partials.alert')
@section('content')



    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-9">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">لیست دسترسی ها</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>شناسه</th>
                                <th>نقش ها</th>
                                <th style="text-align: center">پرمیشن</th>

                            </tr>
                            </thead>




                            @foreach($roles as $role)

                                <tr>
                                    <td> {{  $role->id }} </td>
                                    <td> {{  $role->name}} </td>

                                    <td style="text-align: center">
                                        @foreach($role->permissions as $permission)
                                            <br>
                                            <p class="label label-success">
                                                {{ $permission->name }}
                                            </p>
                                            <br>
                                        @endforeach
                                    </td>

                                </tr>

                            @endforeach










                            <tfoot>
                            <tr>
                                <th>شناسه</th>
                                <th>نقش ها</th>
                                <th style="text-align: center">پرمیشن</th>


                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>

    </table>




@endsection

@include('admin.Partials.footer')