<h6>comment</h6>
<form action="{{ route('site.comment') }}" method="post">
    {!! csrf_field() !!}
    <label>
        <textarea name="body" cols="30" rows="10" class="form-control"></textarea>
    </label>
    <button>submit</button>
    <input type="hidden" name="type" value="{{ $type }}">
    <input type="hidden" name="id" value="{{ $id }}">
</form>