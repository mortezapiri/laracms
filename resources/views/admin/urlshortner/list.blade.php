@include('admin.Partials.header')
@extends('layout.admin')
@include('admin.Partials.sidebar')

@section('title')
    لیست آدرس ها
@endsection

@include('admin.Partials.alert')
@section('content')

    {{--@if($urls && count($url) > 0)--}}

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-9">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"> لیست آدرس ها</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>شناسه</th>
                                    <th>لینک کامل</th>
                                    <th>پسوند ساخته شده</th>
                                    <th>لینک کوتاه شده</th>
                                    <th>زمان ایجاد</th>
                                </tr>
                                </thead>

                                @foreach($urls as $url)

                                    <tr>
                                        <td > {{  $url->url_id }} </td>
                                        <td class="col-md-6"> {{  $url->basic_url }} </td>
                                        <td class="col-md-2"> {{  $url->short_url }} </td>
                                        <td > {{  $url->basic_url }}/{{  $url->short_url }}  </td>
                                        <td class="col-md-2"> {{  $url->created_at }} </td>
                                    </tr>

                                @endforeach




                                <tfoot>
                                <tr>
                                    <th>شناسه</th>
                                    <th>لینک کامل</th>
                                    <th>پسوند ساخته شده</th>
                                    <th>لینک کوتاه شده</th>
                                    <th>زمان ایجاد</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        </div>

        </table>

    {{--@else--}}
        {{--<h1>کاربری برای نمایش وجود ندارد</h1>--}}
        {{--<a href="/admin/users/create"><button class="form-control btn-success col-md-4">اضافه کردن کاربر جدید</button></a>--}}
    {{--@endif--}}




@endsection

@include('admin.Partials.footer')