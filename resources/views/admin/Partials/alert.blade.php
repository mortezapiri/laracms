@if(session('success'))
    <div class="alert alert-success">
        <p>
            {{ session('success') }}
        </p>
    </div><!-- End alert -->
@endif