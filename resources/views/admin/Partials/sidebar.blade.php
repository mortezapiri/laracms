<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-right info">

                <p>{{ Auth::user()->name }}</p>

                <span class="fa fa-circle text-success"></span>
                <span class="fa fa-circle text-danger"></span>

                {{--<p>{{$return}}</p>--}}

                <a href="{{route('admin.general.index')}}" methods="POST" value="online" name="online" class="btn btn-success">آنلاین</a>
                <a href="{{route('admin.general.index')}}" methods="POST" value="offline" name="offline" class="btn btn-danger">آفلاین</a>

                {{--{!! form($form) !!}--}}

            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="جستجو">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">منو</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>کاربران</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.users.create')  }}"><i class="fa fa-circle-o"></i> ایجاد کاربر</a></li>
                    <li><a href="{{  route('admin.users.index')  }}"><i class="fa fa-circle-o"></i>همه کاربران</a></li>
                    <li><a href="{{  route('admin.roles.create')  }}"><i class="fa fa-circle-o"></i>ایجاد نقش</a></li>
                    <li><a href="{{  route('admin.roles.index')  }}"><i class="fa fa-circle-o"></i>نقش ها</a></li>
                    <li><a href="{{  route('admin.permissions.create')  }}"><i class="fa fa-circle-o"></i>ایجاد پرمیشن</a></li>
                    <li><a href="{{  route('admin.permissions.index')  }}"><i class="fa fa-circle-o"></i>لیست پرمیشن ها</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>پست ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.posts.create')  }}"><i class="fa fa-circle-o"></i>افزودن پست جدید</a>
                    </li>
                    {{--<li><a href="{{  route('posts')  }}"><i class="fa fa-circle-o"></i>همه پست ها</a></li>--}}
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>کوتاه کننده لینک</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.urlshortner')  }}"><i class="fa fa-circle-o"></i>ساخت لینک جدید</a>
                    </li>
                    <li><a href="{{  route('admin.posts.create')  }}"><i class="fa fa-circle-o"></i>لیست لینک ها</a>
                    </li>
                </ul>
            </li>


            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-user"></i>--}}
                    {{--<span>آژانس ها</span>--}}
                    {{--<span class="pull-left-container">--}}
              {{--<i class="fa fa-angle-right pull-left"></i>--}}
            {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{  route('admin.agencies.create')  }}"><i class="fa fa-circle-o"></i>افزدون آژانس</a>--}}
                    {{--</li>--}}
                    {{--<li><a href="{{  route('admin.agencies.index')  }}"><i class="fa fa-circle-o"></i>لیست آژانس ها</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>آگهی ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.ads.index')  }}"><i class="fa fa-circle-o"></i>همه اگهی ها</a></li>
                    <li><a href="{{  route('admin.ads.create')  }}"><i class="fa fa-circle-o"></i>افزودن آگهی </a></li>
                    <li><a href="{{  route('admin.ads.create')  }}"><i class="fa fa-circle-o"></i>ویژگی ها </a></li>
                </ul>
            </li>




            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>تور ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.tours.create')  }}"><i class="fa fa-circle-o"></i>افزدون تور</a></li>
                    <li><a href="{{  route('admin.tours.index')  }}"><i class="fa fa-circle-o"></i>لیست تور ها</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>محصولات</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.products.index')  }}"><i class="fa fa-circle-o"></i>محصولات</a></li>
                    <li><a href="{{  route('admin.products.create')  }}"><i class="fa fa-circle-o"></i>افزودن محصول</a></li>
                    <li><a href="{{  route('admin.categories.create')  }}"><i class="fa fa-circle-o"></i>افزودن دسته بندی</a></li>
                    <li><a href="{{  route('admin.categories.index')  }}"><i class="fa fa-circle-o"></i>لیست دسته بندی</a></li>
                    <li><a href="{{  route('admin.attributes.index')  }}"><i class="fa fa-circle-o"></i>افزودن ویژگی</a></li>
                    <li><a href="{{  route('admin.attributes.create')  }}"><i class="fa fa-circle-o"></i>افزودن ویژگی ها</a></li>
                    <li><a href="{{  route('admin.options.create')  }}"><i class="fa fa-circle-o"></i>ایجاد آپشن</a></li>
                </ul>
            </li>


        </ul>

    </section>
    <!-- /.sidebar -->
</aside>


