@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">

                    <form role="form" method="post">

                        {{ csrf_field() }}

                        <div class="form-group">

                            @include('admin.Partials.alert')


                            <label for="full_name">نام کامل</label>
                            <input type="text" class="form-control" id="full_name" name="full_name">

                            <label for="email">ایمیل</label>
                            <input type="email" class="form-control" id="email" name="email">

                            <label for="password">رمز عبور</label>
                            <input type="password" class="form-control" id="password" name="password">

                            <label for="role">نقش کاربری</label>
                            <select name="role" id="role" class="form-control">
                                <option value="1">کاربر عادی</option>
                                <option value="2">اپراتور</option>
                                <option value="3">مدیریت</option>
                            </select>

                            <label for="wallet">موجودی کیف پول :</label>
                            <input type="text" value="0" class="form-control" id="wallet" name="wallet">

                                <button type="submit" class="btn btn-primary" name="submit_save_user">اضافه کردن کاربر</button>

                        </div>



                        {{--<div class="box-body">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputEmail1">ایمیل</label>--}}
                                {{--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="ایمیل">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputPassword1">رمز عبور</label>--}}
                                {{--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="رمز عبور">--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="exampleInputFile">ارسال فایل</label>--}}
                                {{--<input type="file" id="exampleInputFile">--}}

                                {{--<p class="help-block">متن راهنما</p>--}}
                            {{--</div>--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input type="checkbox"> مرا به خاطر بسپار--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!-- /.box-body -->


                    </form>
                </div>
            </div>
        </div>
    </section>
                <!-- /.box -->

@endsection
