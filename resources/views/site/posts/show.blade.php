@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="jumbotron">
            <h1>{{ $post->title }}</h1>

            نویسنده

            <b>{{$post->user->name}}</b>

            <h3>منتشر شده در تاریخ {{$changeDate}}</h3>

            <p>{{ $post->body }}</p>

        </div>

    </div>

@endsection