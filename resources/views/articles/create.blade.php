@include('admin.Partials.header')
@extends('layout.admin')
@include('admin.Partials.sidebar')

@section('content')

    {!! form($form)  !!}

@endsection