
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-right image">
                {{--<img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
            </div>
            <div class="pull-right info">

                خوش آمدی

                {{ Auth::user()->name }}

                {{--<span class="fa fa-circle text-success"></span>--}}
                {{--<span class="fa fa-circle text-danger"></span>--}}

                {{--<p>{{$return}}</p>--}}

                {{--<a href="{{route('admin.general.index')}}" methods="POST" value="online" name="online" class="btn btn-success">آنلاین</a>--}}
                {{--<a href="{{route('admin.general.index')}}" methods="POST" value="offline" name="offline" class="btn btn-danger">آفلاین</a>--}}

                {{--{!! form($form) !!}--}}

            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="جستجو">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">منو</li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>دوره ها</span>
                    <span class="pull-left-container">
              <i class="fa fa-angle-right pull-left"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{  route('admin.posts.create')  }}"><i class="fa fa-circle-o"></i>افزودن دوره جدید</a></li>
                    <li><a href="{{  route('admin.posts.create')  }}"><i class="fa fa-circle-o"></i>لیست دوره ها</a></li>
                </ul>
            </li>





        </ul>

    </section>
    <!-- /.sidebar -->
</aside>


