<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoryOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_option', function (Blueprint $table) {
	        $table->unsignedInteger('category_id');
	        $table->unsignedInteger('option_id');
        });
	    Schema::table('category_option',function (Blueprint $table){
		    $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		    $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_option');
    }
}
