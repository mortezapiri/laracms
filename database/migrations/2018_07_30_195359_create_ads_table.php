<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('content');
            $table->integer('category_id');
            $table->integer('phone');
            $table->string('price');
            $table->string('email');
            $table->string('website');
            $table->string('city');
            $table->string('state');
            $table->integer('likes');
            $table->integer('hits');
			$table->string('images');
			$table->string('aparat_link');
			$table->string('kind_of');
			$table->ipAddress('ip');
			$table->timestamp('expired_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
