<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OptionProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_product', function (Blueprint $table) {
        	$table->unsignedInteger('option_id');
        	$table->unsignedInteger('product_id');
        	$table->longText('data');
        });
        Schema::table('option_product',function (Blueprint $table){
	        $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
	        $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_product');
    }
}
