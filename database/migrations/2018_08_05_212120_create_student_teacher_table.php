<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_teacher', function (Blueprint $table) {
        	$table->unsignedInteger('student_id');
        	$table->unsignedInteger('teacher_id');
        });

	    Schema::table('student_teacher', function (Blueprint $table) {
		    $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
		    $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_teacher');
    }
}
