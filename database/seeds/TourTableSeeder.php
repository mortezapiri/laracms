<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class TourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('tours')->insert( array(
		    'agencie_id' => 1,
		    'title' => 'تور مجارستان',
		    'content' => 'توضیح تور',
		    'hits' => '1',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ));
    }
}
