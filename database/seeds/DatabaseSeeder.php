<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(PermissionTablesSeeder::class);
		$this->call(RolesTableSeeder::class);
		$this->call(AgencyTableSeeder::class);
		$this->call(TourTableSeeder::class);
		$this->call(ProductTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);
		$this->call(AttributeTableSeeder::class);
		// $this->call(UsersTableSeeder::class);
	}
}
