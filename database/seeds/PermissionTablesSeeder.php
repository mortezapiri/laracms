<?php

use App\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PermissionTablesSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

//    	Permission::truncate();

		Permission::create( [
			'name' => 'admin.posts.read',
		] );
		Permission::create( [
			'name' => 'admin.posts.create',
		] );
		Permission::create( [
			'name' => 'admin.posts.update',
		] );
		Permission::create( [
			'name' => 'admin.posts.delete',
		] );
		Permission::create( [
			'name' => 'admin.users.read',
		] );
		Permission::create( [
			'name' => 'admin.users.create',
		] );
		Permission::create( [
			'name' => 'admin.users.update',
		] );
		Permission::create( [
			'name' => 'admin.users.delete',
		] );
		Permission::create( [
			'name' => 'admin.agencies.read',
		] );
		Permission::create( [
			'name' => 'admin.agencies.create',
		] );
		Permission::create( [
			'name' => 'admin.agencies.update',
		] );
		Permission::create( [
			'name' => 'admin.agencies.delete',
		] );
		Permission::create( [
			'name' => 'admin.tours.read',
		] );
		Permission::create( [
			'name' => 'admin.tours.create',
		] );
		Permission::create( [
			'name' => 'admin.tours.update',
		] );
		Permission::create( [
			'name' => 'admin.tours.delete',
		] );
		Permission::create( [
			'name' => 'admin.general.read',
		] );
		Permission::create( [
			'name' => 'dashboard.general.read',
		] );
		Permission::create( [
			'name' => 'admin.permission.read',
		] );
		Permission::create( [
			'name' => 'admin.permission.update',
		] );
		Permission::create( [
			'name' => 'admin.permission.create',
		] );
		Permission::create( [
			'name' => 'admin.roles.read',
		] );
		Permission::create( [
			'name' => 'admin.roles.update',
		] );
		Permission::create( [
			'name' => 'admin.roles.create',
		] );


		//profile
		Permission::create( [
			'name' => 'site.profile.index',
		] );

	}
}
