<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::table('roles')->insert([
		    'name' => 'admin',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ]);
	    DB::table('roles')->insert([
		    'name' => 'manager',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ]);
	    DB::table('roles')->insert([
		    'name' => 'user',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ]);
	    DB::table('roles')->insert([
		    'name' => 'استاد',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ]);
	    DB::table('roles')->insert([
		    'name' => 'هنرجو',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ]);
    }
}
