<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('agencies')->insert( array(
		    'title' => 'سلام پرواز',
		    'about' => 'توضیح آژانس',
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ));
    }
}
