<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Product::create( [
		    'title' => 'گوشی سامسونگ',
		    'category_id' => '1'
	    ] );
	    Product::create( [
		    'title' => 'پراید',
		    'category_id' => '2'
	    ] );
    }
}
